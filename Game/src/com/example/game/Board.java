package com.example.game;

import java.io.Serializable;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.Display;

public class Board implements Serializable {

	/**
	 * 
	 */
	private Cell[] cell = new Cell[24];
	private static final long serialVersionUID = 1L;

	private Cell[][] cells;
	private int[][] neighbours;
	private Rect[] rect;

	private int[] horSX;
	private int[] horEX;
	private int[] horY;
	public Board() {

	}

	public Cell[] getCell() {
		return cell;
	}

	public void setCell(Cell[] cell) {
		this.cell = cell;
	}

	public int getId(Point pos) {
		for(int i=0;i<24;i++)
			if(rect[i].contains(pos.x,pos.y))
				return i;
		return -1;
	}

	public void createBoard(int i, Display disp) {
		if (i == 1) {
			this.neighbours = new int[][] { { 0, 9, 0, 1 }, { 1, 4, 0, 2 },
					{ 2, 14, 1, 2 }, { 3, 10, 3, 4 }, { 1, 7, 3, 5 },
					{ 5, 13, 4, 5 }, { 6, 11, 6, 7 }, { 4, 7, 6, 8 },
					{ 8, 12, 7, 8 }, { 0, 21, 9, 10 }, { 3, 18, 9, 11 },
					{ 6, 15, 10, 11 }, { 8, 17, 12, 13 }, { 5, 20, 12, 14 },
					{ 2, 23, 13, 14 }, { 11, 15, 15, 16 }, { 16, 19, 15, 17 },
					{ 12, 17, 16, 17 }, { 10, 18, 18, 19 }, { 16, 22, 18, 20 },
					{ 13, 20, 19, 20 }, { 9, 21, 21, 22 }, { 19, 22, 21, 23 },
					{ 14, 23, 22, 23 } };
			Point size = new Point();
			size.x=1280;
			size.y=800;
			int w = size.x;
			int h = size.y;
			Log.d("screen", Integer.toString(w));
			Log.d("screen", Integer.toString(h));
			int p;
			p = w / 10;
			this.horSX = new int[] { p * 1, p * 2, p * 3, p * 1,
					p * 3 + (w - p * 6), p * 3, p * 2, p * 1 };
			this.horEX = new int[] { p * 1 + (w - p * 2), p * 2 + (w - p * 4),
					p * 3 + (w - p * 6), p * 3, p * 1 + (w - p * 2),
					p * 3 + (w - p * 6), p * 2 + (w - p * 4),
					p * 1 + (w - p * 2) };
			p = h / 10;
			this.horY = new int[] { p * 1, p * 2, p * 3, p * 4, p * 4, p * 5,
					p * 6, p * 7 };

			int d = 15;
			if (w != 480) {
				float rate = 480 / (float) w;
				float dd = (rate < 1) ? 15 * rate : -15 / rate;
				d += (int) dd;
			}
			this.rect = new Rect[24];
			for (int j = 0, k = 0; j < 9; j += 3, k++) {
				rect[j] = new Rect(horSX[k], horY[k], horSX[k] + 2 * d, horY[k]
						+ 2 * d);
				rect[j + 1] = new Rect((horSX[k] + horEX[k]) / 2 - d, horY[k],
						(horSX[k] + horEX[k]) / 2 + d, horY[k] + 2 * d);
				rect[j + 2] = new Rect(horEX[k] - 2 * d, horY[k], horEX[k],
						horY[k] + 2 * d);
			}
			for (int j = 15, k = 5; j < 24; j += 3, k++) {
				rect[j] = new Rect(horSX[k], horY[k] - 2 * d, horSX[k] + 2 * d,
						horY[k]);
				rect[j + 1] = new Rect((horSX[k] + horEX[k]) / 2 - d, horY[k]
						- 2 * d, (horSX[k] + horEX[k]) / 2 + d, horY[k]);
				rect[j + 2] = new Rect(horEX[k] - 2 * d, horY[k] - 2 * d,
						horEX[k], horY[k]);
			}
			rect[9] = new Rect(horSX[3], horY[3] - d, horSX[3] + 2 * d, horY[3]
					+ d);
			rect[10] = new Rect((horSX[3] + horEX[3]) / 2, horY[3] - d,
					(horSX[3] + horEX[3]) / 2 + 2 * d, horY[3] + d);
			rect[11] = new Rect(horEX[3], horY[3] - d, horEX[3] + 2 * d,
					horY[3] + d);
			rect[12] = new Rect(horSX[4] - 2 * d, horY[4] - d, horSX[4],
					horY[4] + d);
			rect[13] = new Rect((horSX[4] + horEX[4]) / 2 - 2 * d, horY[4] - d,
					(horSX[4] + horEX[4]) / 2, horY[4] + d);
			rect[14] = new Rect(horEX[4] - 2 * d, horY[4] - d, horEX[4],
					horY[4] + d);
		}
		if (i == 2) {

		}
		if (i == 3) {

		}
	}

	public Cell[][] getCells() {
		return cells;
	}

	public void setCells(Cell[][] cells) {
		this.cells = cells;
	}

	public int[][] getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(int[][] neighbours) {
		this.neighbours = neighbours;
	}

	public int[] getHorSX() {
		return horSX;
	}

	public int[] getHorEX() {
		return horEX;
	}

	public void setHorSX(int[] horSX) {
		this.horSX = horSX;
	}

	public void setHorEX(int[] horEX) {
		this.horEX = horEX;
	}

	public int[] getHorY() {
		return horY;
	}

	public void setHorY(int[] horY) {
		this.horY = horY;
	}

	public Rect[] getRect() {
		return rect;
	}

}
