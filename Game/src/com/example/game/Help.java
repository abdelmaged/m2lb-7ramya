package com.example.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class Help extends Activity implements OnClickListener{
	ImageButton Return;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        Return = (ImageButton) findViewById(R.id.HelpReturn);
		//add click listener to buttons
        Return.setOnClickListener(this);        
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	      switch (v.getId()) {
	         case R.id.HelpReturn:
	             Intent Help = new Intent(Help.this, main.class);
	             startActivity(Help);
		      break;	
	      }		
	}
}