package com.example.game;

import java.util.Arrays;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class Player {
	private int[] coins;
	private int pushed_coins = 0;
	private int nCoins = 0;
	private Boolean human = true;
	private Drawable drawable;
	private int[][] neighbours;
	private final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3;

	public Player(Board b) {
		coins = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1 };
		this.neighbours = b.getNeighbours();
	}

	public Boolean finishedPush() {
		if (pushed_coins == 9)
			return true;
		return false;
	}

	public Drawable getDrawable() {
		return drawable;
	}

	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}

	public void pushCoin(int cellID) {
		coins[pushed_coins++] = cellID;
		nCoins++;
	}

	public void clearCell(int cellID) {
		int cc = findCellInCoins(cellID);
		coins[cc] = -1;
		nCoins--;
	}

	public int getnCoins() {
		return nCoins;
	}

	public void setnCoins(int nCoins) {
		this.nCoins = nCoins;
	}

	public void drawCoins(Canvas canvas, final Rect[] r) {
		for (int i = 0; i < pushed_coins; i++) {
			if (coins[i] != -1) {
				this.drawable.setBounds(r[coins[i]]);
				this.drawable.draw(canvas);
			}
		}
	}

	public int findCellInCoins(int cellID) {
		for (int i = 0; i < pushed_coins; i++)
			if (coins[i] == cellID)
				return i;
		return -1;
	}

	public Boolean moveCoin(int from, int to) {
		for (int i = 0; i < pushed_coins; i++) {
			if (coins[i] == from) {
				if (nCoins > 3) {
					for (int k = 0; k < 4; k++)
						if (neighbours[coins[i]][k] == to) {
							coins[i] = to;
							return true;
						}
					return false;
				} else {
					coins[i] = to;
					return true;
				}
			}
		}
		return false;
	}

	public Boolean canEat(int cc){
		int up = neighbours[cc][UP];
		int down = neighbours[cc][DOWN];
		int left = neighbours[cc][LEFT];
		int right = neighbours[cc][RIGHT];
		up 		=  (up 	  != cc && (findCellInCoins(up)    == -1) )? -1 : up;
		down 	=  (down  != cc && (findCellInCoins(down)  == -1) )? -1 : down;
		left 	=  (left  != cc && (findCellInCoins(left)  == -1) )? -1 : left;
		right 	=  (right != cc && (findCellInCoins(right) == -1) )? -1 : right;
		
		if(up != -1){
			if(neighbours[up][UP] != up && findCellInCoins(neighbours[up][UP]) != -1)
				return true;
			if(down != -1)
				return true;
		}
		if(down != -1){
			if(neighbours[down][DOWN] != down && findCellInCoins(neighbours[down][DOWN]) != -1)
				return true;
		}
		if(left != -1){
			if(neighbours[left][LEFT] != left && findCellInCoins(neighbours[left][LEFT]) != -1)
				return true;
			if(right != -1)
				return true;
		}
		if(right != -1){
			if(neighbours[right][RIGHT] != right && findCellInCoins(neighbours[right][RIGHT]) != -1)
				return true;
		}
		return false;
	}
	public Boolean canPlay(Player opp){
		if(pushed_coins != 9)
			return true;
		for(int i=0; i<24; i++){
			if(coins[i] != -1){
				if(opp.findCellInCoins(neighbours[coins[i]][UP]) == -1)
					return true;
				if(opp.findCellInCoins(neighbours[coins[i]][DOWN]) == -1)
					return true;
				if(opp.findCellInCoins(neighbours[coins[i]][LEFT]) == -1)
					return true;
				if(opp.findCellInCoins(neighbours[coins[i]][RIGHT]) == -1)
					return true;
			}
		}
		
		return false;
	}
}
