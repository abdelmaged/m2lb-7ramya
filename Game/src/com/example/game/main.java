package com.example.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class main extends Activity implements OnClickListener{
	ImageButton MainNewGame,MainMultiplayer,MainHelp,MainExit;
	Button p1;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		//define buttons
		MainNewGame = (ImageButton) findViewById(R.id.MainNewGame);
		MainMultiplayer = (ImageButton) findViewById(R.id.MainMultiplayer);
		MainHelp = (ImageButton) findViewById(R.id.MainHelp);
		MainExit = (ImageButton) findViewById(R.id.MainExit);
		//add click listener to buttons
		MainNewGame.setOnClickListener(this);
		MainMultiplayer.setOnClickListener(this);
		MainHelp.setOnClickListener(this);
		MainExit.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	      switch (v.getId()) {
	         case R.id.MainNewGame: 
	          // do something
	        	 Intent Select = new Intent(main.this , SelectLevel.class);
	        	 startActivity(Select);
	          break;
	         case R.id.MainMultiplayer:
	          // do something else
	          break;
	         case R.id.MainHelp:
	             Intent Help = new Intent(main.this, Help.class);
	             startActivity(Help);
		      break;	
	         case R.id.MainExit:
	        	 android.os.Process.killProcess(android.os.Process.myPid());
	        	 super.onDestroy();
		      break;
	      }		
	}
}
